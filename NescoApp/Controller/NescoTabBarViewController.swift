//
//  NescoTabBarViewController.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material

class NescoTabBarViewController: PageTabBarController {

    static let green = UIColor(red: 0/255, green: 121/255, blue: 107/255, alpha: 1)
    open override func prepare() {
        super.prepare()
        preparePageTabBar()
    }
    
    private func preparePageTabBar() {
        //pageTabBar.lineColor = LoginTabBarViewController.appRed
        pageTabBar.backgroundColor = NescoTabBarViewController.green
        pageTabBar.tintColor = Color.clear
        self.pageTabBarAlignment = PageTabBarAlignment.top
        pageTabBar.lineAlignment = .bottom
        pageTabBar.lineColor = UIColor.white
        pageTabBar.dividerColor = Color.clear
        pageTabBar.dividerThickness = 0
        pageTabBar.borderWidth = 0
        
    }
}

extension NescoTabBarViewController: PageTabBarControllerDelegate {
    func pageTabBarController(pageTabBarController: PageTabBarController, didTransitionTo viewController: UIViewController) {
        
        
    }
    
}

