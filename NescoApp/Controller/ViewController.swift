//
//  ViewController.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material

class ViewController: UIViewController {
    
   static let green = UIColor(red: 0/255, green: 121/255, blue: 108/255, alpha: 1)
    @IBOutlet var popUpView1: UIView!
    
    @IBOutlet var engBtn: UIButton!
    
    @IBOutlet var germnBtn: UIButton!
    
    var selctedLanguageImage = false {
        
        didSet {
            
            if (selctedLanguageImage == true) {
                engBtn.backgroundColor = ViewController.green
                germnBtn.backgroundColor = .white
            }
            else {
                germnBtn.backgroundColor = ViewController.green
                engBtn.backgroundColor = .white
            }
        }
    }
    
    var selctedLanguageNewImage = false {
        
        didSet {
            if (selctedLanguageNewImage == true) {
                germnBtn.backgroundColor = ViewController.green
                engBtn.backgroundColor = .white
            }
            else {
                engBtn.backgroundColor = ViewController.green
                germnBtn.backgroundColor = .white
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
       
    }
    
    @IBAction func actionOnEnglishBtn(_ sender: Any) {
        
        self.selctedLanguageImage =  !self.selctedLanguageImage;
        
        if engBtn.backgroundColor == ViewController.green{
            
            L102Language.setAppleLAnguageTo(lang: "en")
        }
    }
    
    @IBAction func actionOnGrmnBtn(_ sender: Any) {
        
        self.selctedLanguageNewImage =  !self.selctedLanguageNewImage;
        
        if germnBtn.backgroundColor == ViewController.green{
            
            L102Language.setAppleLAnguageTo(lang: "de")
        }
       
    }
   
    
    @IBAction func actionOnCancel(_ sender: Any) {
        
        L102Language.setAppleLAnguageTo(lang: "en")
        
        let firstController: ListOfContactViewController =
        {  return UIStoryboard.viewController(identifier: "ListOfContactViewController") as! ListOfContactViewController
        }()
        firstController.tap = true
        if(firstController.tabBar != nil) {
            firstController.tabBar.selectedIndex = 3;
        }
        
        let appToolbarController1 = AppToolbarController(rootViewController: firstController)
        
        windowGlobal!.rootViewController = appToolbarController1
        windowGlobal!.makeKeyAndVisible()
        
    }
    
    @IBAction func actionOnOKBtn(_ sender: Any) {
        
        if engBtn.backgroundColor == ViewController.green{
            self.changeToLanguage("en")
//            L102Language.setAppleLAnguageTo(lang: "en")
        let firstController: ListOfContactViewController =
        {  return UIStoryboard.viewController(identifier: "ListOfContactViewController") as! ListOfContactViewController
        }()
        firstController.tap = true
        if(firstController.tabBar != nil) {
            firstController.tabBar.selectedIndex = 3;
        }
        
        let appToolbarController1 = AppToolbarController(rootViewController: firstController)
        
        windowGlobal!.rootViewController = appToolbarController1
        windowGlobal!.makeKeyAndVisible()
        }
       else if germnBtn.backgroundColor == ViewController.green{
             self.changeToLanguage("de")
            //L102Language.setAppleLAnguageTo(lang: "de")
            let firstController: ListOfContactViewController =
            {  return UIStoryboard.viewController(identifier: "ListOfContactViewController") as! ListOfContactViewController
            }()
            firstController.tap = true
            if(firstController.tabBar != nil) {
                firstController.tabBar.selectedIndex = 3;
            }
            
            let appToolbarController1 = AppToolbarController(rootViewController: firstController)
            
            windowGlobal!.rootViewController = appToolbarController1
            windowGlobal!.makeKeyAndVisible()
        }
        else{
            self.createAlert(title: "", body: "Please select one language")
        }
        
    }
   
    func changeToLanguage(_ langCode: String){
     
        if Bundle.main.preferredLocalizations.first != langCode {
            let confirmAlertCtrl = UIAlertController(title: NSLocalizedString("Want to change the language!", comment: ""), message: NSLocalizedString("Please restart the application", comment: ""), preferredStyle: .alert)
            
            let confirmAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .destructive){_ in
                UserDefaults.standard.set([langCode], forKey: "AppleLanguage")//set([langCode], forKey: "AppleLanguage")
                UserDefaults.standard.synchronize()
                exit(EXIT_SUCCESS)
                
            }
            
            confirmAlertCtrl.addAction(confirmAction)
            let cancelAction = UIAlertAction(title: NSLocalizedString("CANCEL", comment: ""), style: .cancel, handler: nil)
            confirmAlertCtrl.addAction(cancelAction)
            present(confirmAlertCtrl, animated: true, completion: nil)
            
        }
        
        
    }
    
  }
let APPLE_LANGUAGE_KEY = "AppleLanguages"

class L102Language {
    
    /// get current Apple language
    
    class func currentAppleLanguage() -> String{
        
        let userdef = UserDefaults.standard
        
        let langArray = userdef.object(forKey: APPLE_LANGUAGE_KEY) as! NSArray
        
        let current = langArray.firstObject as! String
        
        return current
        
    }
    
    /// set @lang to be the first in Applelanguages list
    
    class func setAppleLAnguageTo(lang: String) {
        
        let userdef = UserDefaults.standard
        
        userdef.set([lang,currentAppleLanguage()], forKey: APPLE_LANGUAGE_KEY)
        
        userdef.synchronize()
        
    }
}
extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
