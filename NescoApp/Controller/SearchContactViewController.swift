//
//  SearchContactViewController.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import ContactsUI

class SearchContactViewController: UIViewController,CNContactPickerDelegate {

    static let green = UIColor(red: 0/255, green: 121/255, blue: 108/255, alpha: 1)
    
    
    @IBOutlet weak var contactLstBtn: UIButton!
   
    
    @IBOutlet weak var sendBtn: UIButton!
    
    @IBOutlet weak var messageField: UITextField!
    
    var messageComposer = MessageComposer()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        preparePageTabBarItem()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        preparePageTabBarItem()
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
       contactLstBtn.backgroundColor = SearchContactViewController.green
        sendBtn.backgroundColor = SearchContactViewController.green
        
    }
    func preparePageTabBarItem() {
        
        pageTabBarItem.image = #imageLiteral(resourceName: "send")
        pageTabBarItem.titleEdgeInsets = UIEdgeInsetsMake(26,-60, 0, 3)
        pageTabBarItem.setTitle(NSLocalizedString("Network", comment: "your network view"), for: .normal)
        // pageTabBarItem.titleLabel!.font =  UIFont(name: "Dienste", size: 15)
        pageTabBarItem.imageEdgeInsets = UIEdgeInsetsMake(4,20,20,20)
        //pageTabBarItem.imageEdgeInsets = UIEdgeInsetsMake(12, 12, 12, 12)
        pageTabBarItem.imageView?.contentMode = .scaleAspectFit
        // pageTabBarItem.titleColor = .white
        //pageTabBarItem.titleLabel?.font = UIFont.appTextFont
        // pageTabBarItem.isHighlighted = true
        pageTabBarItem.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        //        pageTabBarItem.dividerColor = Color.clear
        
    }

    
    
    @IBAction func actionToViewContacts(_ sender: Any) {
        
        let cnPicker = CNContactPickerViewController()
        cnPicker.delegate = self
        self.present(cnPicker, animated: true, completion: nil)
        

    }
    // MARK:- CNContactPickerDelegate Method
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {
        contacts.forEach { contact in
            for number in contact.phoneNumbers {
                let phoneNumber = number.value
                print(" number is = \(phoneNumber.stringValue)")
//                let mobilePhoneLabels = Set<String>(arrayLiteral: CNLabelPhoneNumberMobile, CNLabelPhoneNumberiPhone, "cell", "mobile")
//                
//                let mobileNumbers = contact.phoneNumbers.filter { mobilePhoneLabels.contains($0.label!) && $0.value is CNPhoneNumber }
//                    .map { ($0.value ).stringValue }
                
                //let mobileNumber = mobileNumbers.first ?? ""
               self.messageField.text! = phoneNumber.stringValue
                
            }
        }
    }
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        print("Cancel Contact Picker")
    }

    
    @IBAction func actionOnSendMsg(_ sender: Any) {
    if (messageComposer.canSendText()) {
    // Obtain a configured MFMessageComposeViewController
    let messageComposeVC = messageComposer.configuredMessageComposeViewController()
    messageComposeVC.recipients = [messageField.text!]
    // Present the configured MFMessageComposeViewController instance
    // Note that the dismissal of the VC will be handled by the messageComposer instance,
    // since it implements the appropriate delegate call-back
    present(messageComposeVC, animated: true, completion: nil)
    } else {
    // Let the user know if his/her device isn't able to send text messages
    let errorAlert = UIAlertView(title: NSLocalizedString("Cannot Send Text Message", comment: "can not send the text message"), message: NSLocalizedString("Cannot Send Text Message", comment: "can not send the text message"), delegate: self, cancelButtonTitle: "OK")
//    let errorAlert = UIAlertView(title: "Cannot Send Text Message", message: "Your device is not able to send text messages.", delegate: self, cancelButtonTitle: "OK")
    errorAlert.show()
    }
    
}





    }





