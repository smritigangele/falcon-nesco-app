//
//  ListOfContactViewController.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material

class ListOfContactViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var statusView: UIView!
    static let green = UIColor(red: 0/255, green: 121/255, blue: 108/255, alpha: 1)
    var tap = false
    var tabBar :NescoTabBarViewController!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareStatusBar()
        
        statusView.backgroundColor = ListOfContactViewController.green
        if UserDefaults.standard.string(forKey: ProfileViewController.email) != nil{
//              if tap == true{
            
        Loader.startLoading(message: NSLocalizedString("Please wait...", comment: "search for network"))
            
        let editProfile: EditProfileViewController = storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        
//        let phoneBook: PhoneBookViewController = storyboard?.instantiateViewController(withIdentifier: "PhoneBookViewController") as! PhoneBookViewController
        
        let choosePhone: ChooseContactViewController = storyboard?.instantiateViewController(withIdentifier: "ChooseContactViewController") as! ChooseContactViewController
        
        let profile: ProfileViewController = storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        
        let searchContact: SearchContactViewController = storyboard?.instantiateViewController(withIdentifier: "SearchContactViewController") as! SearchContactViewController
        
        let chef: NescoTabBarViewController = NescoTabBarViewController(viewControllers: [editProfile, choosePhone,searchContact,profile], selectedIndex: 0)
//                chef.selectedIndex = 0
           // chef.pageTabBar.width = 100;
        add(asChildViewController: chef)
            
        }
            else{
               // Loader.startLoading(message: "Bitte Warten...")
                let editProfile: EditProfileViewController = storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
                
//                let phoneBook: PhoneBookViewController = storyboard?.instantiateViewController(withIdentifier: "PhoneBookViewController") as! PhoneBookViewController
            
                let choosePhone: ChooseContactViewController = storyboard?.instantiateViewController(withIdentifier: "ChooseContactViewController") as! ChooseContactViewController
                
                let profile: ProfileViewController = storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                
                let searchContact: SearchContactViewController = storyboard?.instantiateViewController(withIdentifier: "SearchContactViewController") as! SearchContactViewController
                
                let chef: NescoTabBarViewController = NescoTabBarViewController(viewControllers: [editProfile,choosePhone,searchContact,profile], selectedIndex: 3)
                add(asChildViewController: chef)
                chef.selectedIndex = 3
            
            }
        }
    
    fileprivate func prepareStatusBar(){
   
        
    }
    private func add(asChildViewController viewController: UIViewController) {
        addChildViewController(viewController)
        
        containerView.addSubview(viewController.view)
        viewController.view.frame = containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //viewController.view.borderColor = .red
        viewController.didMove(toParentViewController: self)
        
}
}
