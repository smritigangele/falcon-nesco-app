//
//  ConfirmViewCell.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 27/10/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
import Material

class ConfirmViewCell: HTableViewCell{
    
    @IBOutlet weak var confirmOutlineLbl: UILabel!
    @IBOutlet weak var confirmLbl: UILabel!
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layoutMargins = UIEdgeInsets.zero
        separatorInset = UIEdgeInsets.zero
    }
    
}
