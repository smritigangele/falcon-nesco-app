//
//  AssistentCell.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 17/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit

class AssistentCell: UITableViewCell{
    
    
    @IBOutlet weak var imageViewOfItem: UIImageView!
    
    
    @IBOutlet weak var nameOfItem: UILabel!
    
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var time: UILabel!
    
    
    @IBOutlet weak var messageView: UIImageView!
}
