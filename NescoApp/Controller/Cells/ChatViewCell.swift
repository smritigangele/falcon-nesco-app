//
//  ChatViewCell.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 21/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
import Material

class ChatViewCell: HTableViewCell{
    
    @IBOutlet weak var messageFrom: UILabel!
    
    
    @IBOutlet weak var fromDate: UILabel!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layoutMargins = UIEdgeInsets.zero
        separatorInset = UIEdgeInsets.zero
    }
    
    public func styling() {
        messageFrom.font = UIFont.appTextFont
        //IncommingMsg.textColor = .white
        
    }
    
    func updateView(text: GetChatMsgListDTO) {
        
        messageFrom.text! = text.message
        fromDate.text! = text.time
        
    }
}
