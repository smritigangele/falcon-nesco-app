//
//  AddMessagesCell.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 21/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
import Material

class AddMessagesCell: HTableViewCell{
    
    
    @IBOutlet weak var messageTo: UILabel!
    //@IBOutlet weak var confirmOutline: UILabel!
    @IBOutlet weak var toDate: UILabel!
    
//    @IBOutlet weak var bottomConstraintConfirmOutline: NSLayoutConstraint!
//    @IBOutlet weak var topConstraintOfConfirmOutline: NSLayoutConstraint!
//    @IBOutlet weak var confirmLblHeight: NSLayoutConstraint!
//    @IBOutlet weak var confirmLbl: UILabel!
//
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layoutMargins = UIEdgeInsets.zero
        separatorInset = UIEdgeInsets.zero
    }
    
    public func styling() {
        messageTo.font = UIFont.appTextFont
        toDate.font = UIFont.appMinuteFont
    }
    
    func updateView(text: GetChatMsgListDTO) {
        
        messageTo.text! =  text.message.trimmingCharacters(in: .whitespacesAndNewlines) + " "
        print("messages:- \(messageTo.text!)")
        toDate.text! = text.time
        
        //"hello there!!" + "......." +
        
    }
}
