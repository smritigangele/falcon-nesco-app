//
//  PhoneBookCell.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 19/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit


class PhoneBookCell: UITableViewCell{

    
    @IBOutlet weak var nameOfUser: UILabel!
    
    @IBOutlet weak var userLocation: UILabel!
    
}
