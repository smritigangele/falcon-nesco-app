//
//  NescoCell.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 17/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit

class NescoCell: UITableViewCell{
    
    
    @IBOutlet weak var nameField: UILabel!
    
    @IBOutlet weak var mobile: UILabel!
    
    @IBOutlet weak var addBtn: UIButton!
    
    
    @IBOutlet weak var msgField: UILabel!
    
}
