//
//  EditProfileViewController.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material
import Kingfisher

class EditProfileViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var getData = AssistentDTO()
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        preparePageTabBarItem()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        preparePageTabBarItem()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        didRegisterForDevice()
    
    }
    
    func didRegisterForDevice(){
        if UserDefaults.standard.string(forKey: ProfileViewController.email) != nil{
        let getDeviceInfo = RegisterDeviceVM()
        getDeviceInfo.user_type = "A"
        getDeviceInfo.device_id = UserDefaults.standard.string(forKey: AppDelegate.fcm_token)!
        getDeviceInfo.email = UserDefaults.standard.string(forKey: ProfileViewController.email)!
        getDeviceInfo.google_key = "AIzaSyDxwRO0rahaNUBsbK62FzZPupZWMGAa5GQ"
        getDeviceInfo.device_type = "IOS";
        NescoService.updateDevice(view: self, assistent: getDeviceInfo){
            (completion: AssistentDTO) in
        }
    }
        
    }
    
    
    func get_URL(){
    
    if UserDefaults.standard.string(forKey: ProfileViewController.email) != nil{
        
    NescoService.getAssistent(view: self, assistent: UserDefaults.standard.string(forKey: ProfileViewController.email)!){
            (completion: AssistentDTO) in
            print(completion)
            self.getData = completion
        if self.getData.status == "success"{
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        else{
           self.createAlert(title: NSLocalizedString("Alert!", comment: ""), body: "\(NSLocalizedString("your status is", comment: ""))\(self.getData.status)")
        }
        }
}
        
        else{
         self.tableView.alpha = 0
         self.createAlert(title: NSLocalizedString("Alert!", comment: "for login"), body: NSLocalizedString("You have to login first", comment: "body for login"))
            
        }

       }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//     Loader.startLoading(minimumDisplayTime: 1.0, message: "Bitte Warten...");       
        get_URL()
    }
    
    //Aufgaben"
    
    func preparePageTabBarItem() {
        pageTabBarItem.image = #imageLiteral(resourceName: "vernetezen")
        pageTabBarItem.titleEdgeInsets = UIEdgeInsetsMake(26, -60, 0, 3)
        pageTabBarItem.setTitle(NSLocalizedString("Tasks", comment: "your task list"), for: .normal)
        // pageTabBarItem.titleLabel!.font =  UIFont(name: "Dienste", size: 15)
        pageTabBarItem.imageEdgeInsets = UIEdgeInsetsMake(4,20,20,20)
        //pageTabBarItem.imageEdgeInsets = UIEdgeInsetsMake(12, 12, 12, 12)
        pageTabBarItem.imageView?.contentMode = .scaleAspectFit
//        pageTabBarItem.tintColor = .white
        // pageTabBarItem.titleColor = .white
        //pageTabBarItem.titleLabel?.font = UIFont.appTextFont
        // pageTabBarItem.isHighlighted = true
        pageTabBarItem.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        //        pageTabBarItem.dividerColor = Color.clear
    }
}
extension EditProfileViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getData.result.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AssistentCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AssistentCell
        let url = URL(string: self.getData.result[indexPath.row].logo)
         cell.imageViewOfItem.kf.setImage(with: url)
        
        if self.getData.result[indexPath.row].logo == ""{
            
            cell.imageViewOfItem.image = #imageLiteral(resourceName: "logo_1")
        }
        if L102Language.currentAppleLanguage() == "en"{
        cell.nameOfItem.text = self.getData.result[indexPath.row].category_eng
        }
        else if L102Language.currentAppleLanguage() == "de"{
        cell.nameOfItem.text = self.getData.result[indexPath.row].category
        }
        cell.date.text = self.getData.result[indexPath.row].date
        cell.time.text = self.getData.result[indexPath.row].time
        print("Assitent id \(self.getData.result[indexPath.row].assistent_id)");
        print(UserDefaults.standard.object(forKey: "\(self.getData.result[indexPath.row].assistent_id)") ?? "dfyhujk")
        
        if UserDefaults.standard.object(forKey: "\(self.getData.result[indexPath.row].assistent_id)") != nil{
            
            print("")
            
            cell.messageView.image = UIImage(named: "chat")
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "NescoDetailViewController") as! NescoDetailViewController
        vc.data = self.getData.result[indexPath.row].assistent_id
        
        present(vc, animated: true, completion: nil)
        
        }
    
//     vc.products = buyerPaginateDTO.data[(tableViewForBuyer.indexPath(for:(sender.superview?.superview) as! BuyerTableViewCell)?.row)!].product
    
}


    
