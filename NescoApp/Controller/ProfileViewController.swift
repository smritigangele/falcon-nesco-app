//
//  ProfileViewController.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 17/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
  static let green = UIColor(red: 0/255, green: 121/255, blue: 108/255, alpha: 1)
    
    static let email = "email"
     static let assistent_id = "assistent_id"
   
    
    @IBOutlet weak var logoutBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var emailField: HTextField!
    
    @IBOutlet weak var passwordField: HTextField!

 
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        preparePageTabBarItem()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        preparePageTabBarItem()
    }

    func preparePageTabBarItem() {
        //NSLocalizedString("Profile", comment: "your profile view")
        
        pageTabBarItem.image = #imageLiteral(resourceName: "userProfile")
        pageTabBarItem.titleEdgeInsets = UIEdgeInsetsMake(26, -60, 0, 3)
//        if L102Language.currentAppleLanguage() == "en"{
//            pageTabBarItem.setTitle("Profile".localized, for: .normal)
//            //NSLocalizedString("Profile", comment: "")
//            //L102Language.LanguageManager.sharedInstance.LocalizedLanguage(key: "Profile", languageCode: "en")
//        }
//
//        else if L102Language.currentAppleLanguage() == "de"{
        
        pageTabBarItem.setTitle("Profile".localized, for: .normal)
//        L102Language.LanguageManager.sharedInstance.LocalizedLanguage(key: "Profile", languageCode: "de")
//        }
        
        pageTabBarItem.imageEdgeInsets = UIEdgeInsetsMake(4,20,20,20)
        pageTabBarItem.imageView?.contentMode = .scaleAspectFit
        pageTabBarItem.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.loginBtn.backgroundColor = ProfileViewController.green
        
      if UserDefaults.standard.string(forKey: ProfileViewController.email) != nil{

        self.emailField.isUserInteractionEnabled = false
        self.passwordField.isUserInteractionEnabled = false
        self.loginBtn.isUserInteractionEnabled = false

        }
        
      else{
        
        self.emailField.isUserInteractionEnabled = true
        self.passwordField.isUserInteractionEnabled = true
        self.loginBtn.isUserInteractionEnabled = true
        
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
            loginBtn.isHidden = false
            logoutBtn.isHidden = false

    }
    
    @IBAction func actionOnBtn(_ sender: Any) {
        
        let login = LoginVM()
        login.email = self.emailField.text!
        login.password = self.passwordField.text!
        
        NescoService.login(view: self, assistent: login){ (completion: LoginDTO) in
            if completion.status == "fail"{
                
            self.createAlert(title: NSLocalizedString("Alert!", comment: ""), body: "\(NSLocalizedString("your status is", comment: ""))\(completion.status)")
            }
                
            else{
                
                if self.emailField.text! == ""{
                   self.createAlert(title: "", body: NSLocalizedString("Email field is empty!!", comment: ""))
                    return
                }
                if self.passwordField.text! == ""{
                  self.createAlert(title: "", body: NSLocalizedString("Please enter password", comment: ""))
                    return
                }
                    
                else{
           UserDefaults.standard.setValue(completion.result[0].email, forKey: ProfileViewController.email)
            UserDefaults.standard.setValue(completion.result[0].company, forKey: "companyName")
            UserDefaults.standard.setValue(completion.result[0].assistent_id, forKey: ProfileViewController.assistent_id)
            UserDefaults.standard.synchronize()
            self.createAlert(title: "", body: NSLocalizedString("Success!", comment: ""))//completion.status)
            
            let firstController: ListOfContactViewController =
                {  return UIStoryboard.viewController(identifier: "ListOfContactViewController") as! ListOfContactViewController
            }()
            firstController.tap = true
            if(firstController.tabBar != nil) {
                firstController.tabBar.selectedIndex = 0;
            }

            let appToolbarController1 = AppToolbarController(rootViewController: firstController)
            
            windowGlobal!.rootViewController = appToolbarController1
            windowGlobal!.makeKeyAndVisible()
                }
            }
        }
   }
    
    @IBAction func actionOnLgout(_ sender: Any) {
        
      Service.logout(view: self)
    
       emailField.text! = ""
       passwordField.text! = ""
     if emailField.text! == ""{
         self.createAlert(title: "", body: NSLocalizedString("Logged out successfully!", comment: ""))
        }
     
    self.createAlert(title: "", body: NSLocalizedString("Logged out successfully!", comment: ""))
        
    }
}


//    class Localization{
//    //
//        func localized(lang:String) ->String {
////
//            let path = Bundle.main.path(forResource: lang, ofType: "lproj")
//            let bundle = Bundle(path: path!)
//
//
//            return NSLocalizedString(<#T##key: String##String#>, comment: <#T##String#>)//(self, tableName: nil, bundle: bundle!, value: "", comment: "")
//        }
//    }
   // class LanguageManager
//    {
//
//        static let sharedInstance =  LanguageManager()
//
//        //Make a function for Localization. Language Code is the one which we will be deciding on button click.
//        func LocalizedLanguage(key:String,languageCode:String)->String{
//
//            //Make sure you have Localizable bundles for specific languages.
//            var path = Bundle.main.path(forResource: languageCode, ofType: "lproj")
//
//            //Check if the path is nil, make it as "" (empty path)
//            path = (path != nil ? path:"")
//
//            let languageBundle:Bundle!
//
//            if(FileManager.default.fileExists(atPath: path!)){
//                languageBundle = Bundle(path: path!)
//                return languageBundle!.localizedString(forKey: key, value: "", table: nil)
//            }else{
//                // If path not found, make English as default
//                path = Bundle.main.path(forResource: "en", ofType: "lproj")
//                languageBundle = Bundle(path: path!)
//                return languageBundle!.localizedString(forKey: key, value: "", table: nil)
//            }
//        }
//     }
//L102Language is responsible for getting/setting language from/in the UserDefaults.
//
//Add these lines into the switchLanguage Method


