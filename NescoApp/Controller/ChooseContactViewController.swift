//
//  ChooseContactViewController.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material

class ChooseContactViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var firstName: HTextField!
    @IBOutlet weak var emailField: HTextField!
    @IBOutlet weak var phoneField: HTextField!
    @IBOutlet var frontView: UIView!
    
    @IBOutlet weak var plusBtn: UIButton!
    
    var anyArray: Dictionary<String, String> = Dictionary<String, String>()
    
    var contacts = ContactsDTO()
    var getContacts = GetContactsDTO()
    var tableData = [ContactsDTO]()
    var contents = GetContactsDTO()
    let blurrEffect = UIBlurEffect(style: UIBlurEffectStyle.prominent)
    let blurrEffectView = UIVisualEffectView()
    let gesture = UITapGestureRecognizer()
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        preparePageTabBarItem()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
        preparePageTabBarItem()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        plusBtn.imageView?.contentMode = .scaleAspectFit
        
        if self.getContacts.result.count == 0{
         self.tableView.alpha == 0
            
        }
        
    }
    
    func get_URL(){
        
        let assistent = GetAssistentVM()
        if UserDefaults.standard.string(forKey: ProfileViewController.email) != nil{
        assistent.email = UserDefaults.standard.string(forKey: ProfileViewController.email)!
        NescoService.getCustomer_1(view: self, assistent: assistent){
      (completion: GetContactsDTO) in
            print(completion)
            self.getContacts = completion
            if self.getContacts.status == "fail"{
                
                self.createAlert(title: NSLocalizedString("Alert!", comment: ""), body: "\(NSLocalizedString("your status is", comment: ""))\(completion.status)")
            }
            else{
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                      }
                 }
             }
        }
    else{
            self.tableView.alpha = 0
            self.createAlert(title: NSLocalizedString("Alert!", comment: "for login"), body: NSLocalizedString("You have to login first", comment: "body for login"))
        }
}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        get_URL()
    }
    
    func preparePageTabBarItem() {
       pageTabBarItem.image = #imageLiteral(resourceName: "hands")
        pageTabBarItem.titleEdgeInsets = UIEdgeInsetsMake(26, -60, 0, 3)
        pageTabBarItem.setTitle(NSLocalizedString("Contact", comment: "title of the view"), for: .normal)
        // pageTabBarItem.titleLabel!.font =  UIFont(name: "Dienste", size: 15)
        pageTabBarItem.imageEdgeInsets = UIEdgeInsetsMake(4,20,20,20)
         // pageTabBarItem.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
         pageTabBarItem.imageView?.contentMode = .scaleAspectFit
        // pageTabBarItem.titleColor = .white
        //pageTabBarItem.titleLabel?.font = UIFont.appTextFont
        // pageTabBarItem.isHighlighted = true
        pageTabBarItem.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        //        pageTabBarItem.dividerColor = Color.clear
        
    }
    
    @IBAction func actionOnBtn(_ sender: Any) {
        
        self.blurrEffectView.effect = self.blurrEffect
        blurrEffectView.frame = view.bounds
        blurrEffectView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        gesture.addTarget(self, action: #selector(dissmissB(_:)))
        blurrEffectView.isUserInteractionEnabled = true
        blurrEffectView.addGestureRecognizer(gesture)
        self.view.insertSubview(self.blurrEffectView, at: 2)
        //self.view.addSubview(blurrEffectView)
        frontView.center = self.view.center
        frontView.backgroundColor = UIColor.white
        frontView.borderWidth = 1
        frontView.layer.cornerRadius = 3
        frontView.clipsToBounds = true
        frontView.borderColor = UIColor.lightGray
//        blurrEffectView.addSubview(frontView)
        self.blurrEffectView.contentView.insertSubview(frontView, at: 3)
        
    }
    func dissmissB(_ tab:UITapGestureRecognizer){
        tab.view?.removeFromSuperview()
    }


    @IBAction func actionOnContactBtn(_ sender: Any){
        
        let assistent = AddContactsVM()
        assistent.email = UserDefaults.standard.string(forKey: ProfileViewController.email)!
        assistent.user_email = self.emailField.text!
            assistent.phone = self.phoneField.text!
        assistent.name = self.firstName.text!
        
        NescoService.addContact(view: self, assistent: assistent){
            
        (completion: AddContactsDTO) in
            print(completion)
    self.createAlert(title: "", body: completion.result.message)
            
    }
}
}
extension ChooseContactViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getContacts.result.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PhoneBookCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PhoneBookCell
        
        cell.nameOfUser.text = self.getContacts.result[indexPath.row].name
        cell.userLocation.text = self.getContacts.result[indexPath.row].city
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected ......")
    }
    
}
