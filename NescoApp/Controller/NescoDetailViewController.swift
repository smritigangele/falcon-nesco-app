//
//  NescoDetailViewController.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 17/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit

class NescoDetailViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    var data: String = ""
    var getData = ServiceListDTO()
    var notiData = [NotifictnData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

      tableView.delegate = self
        tableView.dataSource = self
        get_URL()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserDefaults.standard.object(forKey: "\(data)") != nil {
        let explodedData = UserDefaults.standard.object(forKey: "\(data)") as! [Data];
        if(explodedData.count>0) {
            for dat in explodedData {
                let notiSmallData = NSKeyedUnarchiver.unarchiveObject(with: dat) as!NotifictnData;
                notiData.append(notiSmallData);
            }
         }
        }

    }
    
    
    @IBAction func actionOnBckBtn(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func get_URL(){
        
        //let userId = GetAssistentVM()
        
        NescoService.getMssgs(view: self, assistent: data){
            (completion: ServiceListDTO) in
            
            print(completion)
            self.getData = completion
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
    }
}

}
extension NescoDetailViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getData.result.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: NescoCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NescoCell
        cell.nameField.text = self.getData.result[indexPath.row].firstname
        cell.mobile.text = self.getData.result[indexPath.row].phone
        cell.addBtn.addTarget(self, action: #selector(handleCell(_ :)), for: .touchUpInside)
        
        let userid = self.getData.result[indexPath.row].id
        for noti in notiData {
            if noti.user_id == userid {
                cell.msgField.text = noti.message
                break;
            }
            
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        vc.assistent_id = self.data
        vc.getData = self.getData.result[indexPath.row]
//        notiData.count = 0;
    
        var explodedData = [Data]();
        if UserDefaults.standard.object(forKey: "\(data)") != nil {
            explodedData = UserDefaults.standard.object(forKey: "\(data)") as! [Data];
        }
        var encodedData = [Data]();
        if(explodedData.count>0) {
            for dat in explodedData {
                let notiSmallData = NSKeyedUnarchiver.unarchiveObject(with: dat) as!NotifictnData;
                if(notiSmallData.user_id != vc.getData.id) {
                    notiData.append(notiSmallData);
                    encodedData.append(dat);
                }
            }
            if(encodedData.count == 0) {
                UserDefaults.standard.removeObject(forKey: "\(self.data)");
            } else {
                UserDefaults.standard.set(encodedData, forKey: "\(self.data)");
            }
            UserDefaults.standard.synchronize();
        }
        
        present(vc, animated: true, completion: nil)
        
    }
    
    func handleCell(_ sender: UIButton ){
        
        let contacts = AddContactsVM()
        contacts.user_email = self.getData.result[(tableView.indexPath(for:(sender.superview?.superview) as! NescoCell)?.row)!].email
        contacts.name = self.getData.result[(tableView.indexPath(for:(sender.superview?.superview) as! NescoCell)?.row)!].firstname
        contacts.phone = self.getData.result[(tableView.indexPath(for:(sender.superview?.superview) as! NescoCell)?.row)!].phone
        contacts.email = UserDefaults.standard.string(forKey: ProfileViewController.email)!
        
        NescoService.addContact(view: self, assistent: contacts){ (completion: AddContactsDTO) in
            
            print(completion)
        
        
        }
    }
    
}
