//
//  ChatViewController.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 21/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Kingfisher
import UserNotifications
import Material

var openNewWindow: UIWindow!

class ChatViewController: UIViewController,UNUserNotificationCenterDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageField: UITextView!
    @IBOutlet weak var imageOfItem: UIImageView!
    @IBOutlet weak var nameOfCategory: UILabel!

    var moveBck = false
    var isFromNotification = false
     var asstntID = ""
     var usrID = ""
    var bck = false
    var isFromAgenda = false
    var assistent_id = ""
    var getData = ServiceMssgsDTO()
    var getListOfMessages = GetChatMsgDTO()
    var chatNew = SendChatMsgsVM()
    var newMssgs = AddMsgsDTO()
    let chat = ChatVM()
    var noOfContent = Array<GetChatMsgListDTO>()
    var tableData = [GetDate]()
   var savedData: [[GetChatMsgListDTO]] = [[]];
    var sectionHeader: [String] = [];
    var currentDate = "";

    override func viewDidLoad() {
        super.viewDidLoad()

    tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundView = UIImageView(image: #imageLiteral(resourceName: "bg"))
            nameOfCategory.text! = self.getData.firstname
        self.tableView.estimatedRowHeight = 132.0;
        self.tableView.rowHeight = UITableViewAutomaticDimension
            self.noOfContent.append(contentsOf: getListOfMessages.result)
        
          UNUserNotificationCenter.current().requestAuthorization(options: [.badge,.alert,.sound], completionHandler: {didAllow, error in})
        self.tableView.setNeedsLayout()
        self.tableView.layoutIfNeeded()
    }
          
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
          
          get_URL()
          
          }
      func get_URL(){
          
          if isFromNotification == true{
              chat.assistent_id = Int(asstntID)!
              chat.user_id = Int(usrID)!
          NescoService.getChatMsgs(view: self, assistent: chat){
          (completion: GetChatMsgDTO) in
          print(completion)
          self.getListOfMessages = completion
          DispatchQueue.main.async {
                                        
          self.tableView.reloadData()
                              }
                              
       self.getTableArray()
                    }

          }
          else{
          
           chat.assistent_id = Int(self.assistent_id)! //UserDefaults.standard.integer(forKey: ProfileViewController.assistent_id)
                    chat.user_id = Int(self.getData.id)!
         Loader.startLoading(minimumDisplayTime: 0.3, message: NSLocalizedString("Please wait", comment: "search for network"))
        NescoService.getChatMsgs(view: self, assistent: chat){ (completion: GetChatMsgDTO) in
        print(completion)
        self.getListOfMessages = completion
            
      self.getReloadData()
            
         self.getTableArray()
        
        }
    }
          }
    
   
     func getTableArray()
    {
       savedData = [[]];
       sectionHeader = [];
          var tmpDate = "";
          var index = -1;
              for content in self.getListOfMessages.result {
                    if(content.date != tmpDate) {
                              index += 1;
                              var arr = [GetChatMsgListDTO]();
                              arr.append(content);
                              savedData.append(arr);
                              tmpDate = content.date;
                              sectionHeader.append(tmpDate);
                    } else {
                              savedData[index].append(content);
                    }
                self.getReloadData()
            
        }
//          getMainList()
    }
    
    func getReloadData(){
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
            
            self.moveToLastComment()
        }
//        Loader.stopLoading()
    }
    func getMainList(){
        
            for data in tableData {
                let subDataList = self.getListOfMessages.result.filter({$0.date == data.date})
                    
                for subData in subDataList {
                    let subMenuContent = GetDatesDTO()
                    subMenuContent.date = subData.date
                    data.getData.append(subMenuContent)
                }
            }
    }
    
    @IBAction func actionOnBack(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionOnSendBtn(_ sender: Any) {
        
        let chat = GetChatMsgListDTO()
        if self.messageField.text! == ""{
            
            showNewToast(message: NSLocalizedString("Enter your message", comment: "when sending text is empty"))//"Enter your message")
        }
        else{
        chat.message = self.messageField.text!
        chatNew.user_id = self.getData.id
        //UserDefaults.standard.string(forKey: ProfileViewController.email) as String!
        chatNew.assistent_id = self.assistent_id
        chatNew.message = chat.message
        
        NescoService.sendMsgs(view: self, assistent: chatNew){
            (completion: AddMsgsDTO) in
            self.newMssgs = completion
            print(self.newMssgs)
            self.savedData.append([chat])
            self.get_URL()
                  }
           self.messageField.text = ""
        }
    }
    
    @IBAction func actionOnConfirmedBtn(_ sender: Any) {
        let assistent = ConfirmMsgVM()
        assistent.assistent_id = Int(self.assistent_id)!
        assistent.user_id = Int(self.getData.id)!
        NescoService.confirmBtn(view: self, assistent: assistent){
            
        (completion: ConfirmMsgDTO) in
            print(completion)

            if completion.status == "success"{
                let  sendNewMsg = SendChatMsgsVM()
                
                sendNewMsg.assistent_id = self.assistent_id
                sendNewMsg.user_id = self.getData.id
                sendNewMsg.message = "CONFIRMED"
                
                NescoService.sendMsgs(view: self, assistent:sendNewMsg ){
                    (completion: AddMsgsDTO) in
                 print(completion)
                    
                    self.get_URL()
                //self.getTableArray()
                    
                    
               // self.tableView.reloadData()
                    
           }
        
        }
    }
  }
//          func reloadData(){
//                    tableView.reloadData()
//                    DispatchQueue.main.async{
//                              let scrollPoint = CGPoint(x: 0, y: self.tableView.contentSize.height - self.tableView.frame.size.height)
//                              self.tableView.setContentOffset(scrollPoint, animated: true)
//                    }
//          }
//
    
}
extension ChatViewController: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
          return sectionHeader.count

      //  return tableData.count
    }
          
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sectionCell") as!
        ChatSectionTableViewCell
//          print("Section \(section)")
//          print("self.sectionHeader \(self.sectionHeader)")
        cell.currentDateLbl.text = self.sectionHeader[section]
        cell.contentView.backgroundColor = .white
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionHeader[section]
          }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  savedData[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        

        if self.savedData[indexPath.section][indexPath.row].from_mess == "assestent"{
            
             let cell: AddMessagesCell = tableView.dequeueReusableCell(withIdentifier: "cellForAddMessage", for: indexPath) as! AddMessagesCell
//            cell.messageFrom.numberOfLines=0
//            cell.messageFrom.lineBreakMode = NSLineBreakMode.byWordWrapping
//            cell.messageFrom.text! = savedData[indexPath.section][indexPath.row].message
//
//            cell.fromDate.text = savedData[indexPath.section][indexPath.row].time
//
//            cell.messageFrom.textAlignment = .center
//            cell.messageFrom.sizeToFit()
//            cell.messageFrom.layer.cornerRadius = 3
//            let padding = UIEdgeInsetsMake(16,16,16,16)
//
//            cell.messageFrom.contentMode = .center
//            cell.messageFrom.layoutEdgeInsets = padding
//            cell.messageFrom.clipsToBounds = true
            cell.backgroundView?.backgroundColor = .clear
            cell.selectionStyle = .none
            cell.styling()
            cell.updateView(text: savedData[indexPath.section][indexPath.row])
            print("list is:- \(savedData[indexPath.section][indexPath.row].message)")
//            if savedData[indexPath.section][indexPath.row].message == "CONFIRMED "{
//                print("testing","aaea gea")
//                cell.confirmLbl.isHidden = false
//                cell.confirmLbl.text = "CONFIRMED"
//                cell.confirmLbl.backgroundColor = UIColor(patternImage: UIImage(named: "bg")!)
//                cell.confirmOutline.isHidden = false
//                cell.confirmLblHeight.constant = 30
//                cell.bottomConstraintConfirmOutline.constant = 24
//                cell.topConstraintOfConfirmOutline.constant = 20
//                cell.messageTo.isHidden = true
//                cell.toDate.isHidden = true
//            }
//            else{
//                cell.confirmOutline.isHidden = true
//                cell.confirmLbl.isHidden = true
//                cell.confirmLblHeight.constant = 0
//                cell.bottomConstraintConfirmOutline.constant = 4
//                cell.topConstraintOfConfirmOutline.constant = 0
//            }
        if self.savedData[indexPath.section][indexPath.row].message == "CONFIRMED"{
                let cell: ConfirmViewCell = tableView.dequeueReusableCell(withIdentifier: "cellForConfirm", for: indexPath) as! ConfirmViewCell
                return cell
            }
             return cell
        }
//    else if self.savedData[indexPath.section][indexPath.row].message == "CONFIRMED"{
        
        //return cell
            
       // }
            
    else {
            
          let cell: ChatViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ChatViewCell
        
    //let cell: AddMessagesCell = tableView.dequeueReusableCell(withIdentifier: "cellForAddMessage", for: indexPath) as! AddMessagesCell
//        cell.messageTo.text = savedData[indexPath.section][indexPath.row].message
//            cell.messageTo.lineBreakMode = .byWordWrapping
//
//            let padding = UIEdgeInsetsMake(16,16,16,16)
//            cell.messageTo.contentMode = .center
//            cell.messageTo.layoutEdgeInsets = padding
//            cell.messageTo.numberOfLines = 0
//
//            cell.messageTo.layer.cornerRadius = 3
//            cell.messageTo.clipsToBounds = true
//            cell.messageTo.textAlignment = .center
//            cell.messageTo.sizeToFit()
//
            cell.backgroundView?.backgroundColor = .clear

            cell.selectionStyle = .none
           
            cell.styling()
            cell.updateView(text: savedData[indexPath.section][indexPath.row])
            //cell.toDate.text = savedData[indexPath.section][indexPath.row].time
           
           
             return cell
          }
        
      }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableViewAutomaticDimension
        }
        
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableViewAutomaticDimension
        }
    
    func moveToLastComment() {
      if self.tableView.contentSize.height > self.tableView.frame.height {
                              // First figure out how many sections there are
      let lastSectionIndex = self.tableView!.numberOfSections - 1
                              
                              // Then grab the number of rows in the last section
      let lastRowIndex = self.tableView!.numberOfRows(inSection: lastSectionIndex) - 1
                              
                              // Now just construct the index path
      let pathToLastRow = NSIndexPath(row: lastRowIndex, section: lastSectionIndex)
                              
                              // Make the last row visible
       self.tableView?.scrollToRow(at: pathToLastRow as IndexPath, at: UITableViewScrollPosition.bottom, animated: true)
                    }
          }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("fghjk")
       
    }
    func showNewToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 40, y: self.view.frame.size.height - 100, width: self.view.frame.size.width - 75, height: 48))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.tableView.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}

