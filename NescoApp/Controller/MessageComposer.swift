//
//  MessageComposer.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 06/07/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
import MessageUI


var textMessageRecipients = [""]
class MessageComposer: NSObject, MFMessageComposeViewControllerDelegate{
    
    func canSendText() -> Bool {
        return MFMessageComposeViewController.canSendText()
    }
    
    // Configures and returns a MFMessageComposeViewController instance
    func configuredMessageComposeViewController() -> MFMessageComposeViewController {
        let messageComposeVC = MFMessageComposeViewController()
        messageComposeVC.messageComposeDelegate = self  //  Make sure to set this property to self, so that the controller can be dismissed!
        messageComposeVC.recipients = textMessageRecipients
        //messageComposeVC.
        
        messageComposeVC.body = UserDefaults.standard.string(forKey: "companyName")! + "" + "möchte dir Assistent24 vorstellen" + "https://itunes.apple.com/in/app/assistent-24/id1260118839?mt=8"
        
        return messageComposeVC
    }
    
    // MFMessageComposeViewControllerDelegate callback - dismisses the view controller when the user is finished with it
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}
