//
//  PhoneBookViewController.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import UIKit
import Material

class PhoneBookViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    var registeredUsers = RegisteredUsersDTO()
     var getContacts = ContactsDTO()
    
//    let tabBar = NescoTabBarViewController()
    
    var tableData = [RegisteredUsersDTO]()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        preparePageTabBarItem()
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
            preparePageTabBarItem()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
      // get_URL()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let firstController: ListOfContactViewController =
            {  return UIStoryboard.viewController(identifier: "ListOfContactViewController") as! ListOfContactViewController
        }()
        firstController.tap = true
        if(firstController.tabBar != nil) {
            firstController.tabBar.selectedIndex = 3;
        }
        let appToolbarController1 = AppToolbarController(rootViewController: firstController)
        
        windowGlobal!.rootViewController = appToolbarController1
        windowGlobal!.makeKeyAndVisible()
    
        for window in UIApplication.shared.windows {
            if window.isKeyWindow != true {
                
                window.removeFromSuperview()
            }
        }
    }

    func preparePageTabBarItem() {
       // pageTabBarItem.image = #imageLiteral(resourceName: "hands")
        pageTabBarItem.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10)
        pageTabBarItem.imageView?.contentMode = .scaleAspectFit
        // pageTabBarItem.titleColor = .white
        //pageTabBarItem.titleLabel?.font = UIFont.appTextFont
        // pageTabBarItem.isHighlighted = true
        pageTabBarItem.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        //        pageTabBarItem.dividerColor = Color.clear
        
    }
//    private func add(asChildViewController viewController: UIViewController) {
//        addChildViewController(viewController)
//        
//        containerView.addSubview(viewController.view)
//        viewController.view.frame = containerView.bounds
//        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        //viewController.view.borderColor = .red
//        viewController.didMove(toParentViewController: self)
//        
//    }
    
}
extension PhoneBookViewController: UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected ......")
    }
    
}


