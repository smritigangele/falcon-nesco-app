//
//  NescoService.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 17/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import EVReflection

class NescoService {
    
    
    static func login(view: UIViewController, assistent: LoginVM, completion: @escaping (LoginDTO) -> Void) {
        Service.get(url: Constants.loginURL, parameters: assistent.toJSON(), view: view, completion: completion)
    }
    
    static func getAssistent(view: UIViewController, assistent: String, completion: @escaping (AssistentDTO) -> Void) {
        Service.get(url: Constants.getAssistent+assistent, parameters: [:], view: view, completion: completion)

    }
    static func getRegisteredUsers(view: UIViewController, completion: @escaping (RegisteredUsersDTO) -> Void) {
        
        Service.get(url: Constants.registeredUsers, parameters:[:], view: view, completion: completion)
        
       }

    static func getMssgs(view: UIViewController, assistent: String, completion: @escaping (ServiceListDTO) -> Void)  {
        
        Service.get(url: Constants.getServiceMssgs+assistent, parameters:[:], view: view, completion: completion)
        
    }
    static func addContact(view: UIViewController, assistent: AddContactsVM , completion: @escaping (AddContactsDTO) -> Void)  {
        
        Service.get(url: Constants.addContacts, parameters:assistent.toJSON(), view: view, completion: completion)
        
    }
    static func getChatMsgs(view: UIViewController, assistent: ChatVM , completion: @escaping (GetChatMsgDTO) -> Void)  {
        
        Service.get(url: Constants.getChatMessgs, parameters: assistent.toJSON(), view: view, completion: completion)
        
    }
    
    static func sendMsgs(view: UIViewController, assistent: SendChatMsgsVM , completion: @escaping (AddMsgsDTO) -> Void)  {
               Service.get(url: Constants.sendChatMsgs, parameters: assistent.toJSON(), view: view, completion: completion)
        
    }
    static func confirmBtn(view: UIViewController, assistent: ConfirmMsgVM , completion: @escaping (ConfirmMsgDTO) -> Void)  {
        
        Service.get(url: Constants.confirmMsg, parameters: assistent.toJSON(), view: view, completion: completion)
    }
    static func getContacts(view: UIViewController, completion: @escaping (ContactsDTO) -> Void) {
        Service.get(url: Constants.getCustomer, parameters: [:], view: view, completion: completion)
        
    }
    

    static func getCustomer(view: UIViewController, assistent: GetAssistentVM, completion: @escaping (GetContactsDTO) -> Void)  {
        
        Service.get(url: Constants.myContacts, parameters: assistent.toJSON(), view: view, completion: completion)
        
    }
    static func getCustomer_1(view: UIViewController, assistent: GetAssistentVM, completion: @escaping (GetContactsDTO) -> Void)  {
        
        Service.get(url: Constants.myContacts_1, parameters: assistent.toJSON(), view: view, completion: completion)
        
    }
    static func updateDevice<T:EVObject>(view: UIViewController, assistent: RegisterDeviceVM, completion: @escaping (T) -> Void)  {
        
        Service.get(url: Constants.updateDevice, parameters: assistent.toJSON(), view: view, completion: completion)
        
    }

   


}
