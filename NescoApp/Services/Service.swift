//
//  Service.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 15/09/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import Foundation
import UIKit
import Alamofire
import EVReflection

class Service {
    static let hunger_hub: String = "hunger_hub_chef"
  
    static var userToken: String? = nil
    
    static var authHeader: HTTPHeaders = HTTPHeaders();
    
    private static let sessionManager = Alamofire.SessionManager.default
    
    static func loadTokenOnAppLoad() {
        if let token = UserDefaults.standard.string(forKey: hunger_hub) as String! {
            userToken = token
            authHeader = [
                "Authorization": "Bearer " + userToken!
            ];
        }
        print("User Token : \(userToken)")
    }
    
    static func get<T:EVObject>(url: String, parameters: Dictionary<String, Any> = [String: String](), view : UIViewController,completion: @escaping (T) -> Void) {
        baseNetworkOperation(method: .get, url: url, parameters: parameters,view: view, completion: completion)
    }
    //    static func getArray<T:EVObject>(url: String, parameters: Dictionary<String, Any> = [String: String](), view : UIViewController, authRequired: Bool, completion: @escaping ([T]) -> Void) {
    //        newBaseNetworkOperation(method: .get, url: url, parameters: nil,view: view, authRequired: authRequired,  completion: completion)
    //    }
    static func getBaseNtwrk<T:EVObject>(url: String, parameters: Dictionary<String, Any> = [String: String](), view : UIViewController, completion: @escaping (T) -> Void) {
        baseNetworkOperationMethod(method: .get, url: url, parameters: parameters, view: view, completion: completion)
    }
    
    
    static func post<T:EVObject>(url: String, parameters: Dictionary<String, Any>, view : UIViewController,completion: @escaping (T) -> Void) {
        baseNetworkOperation(method: .post, url: url, parameters: parameters, encoding: JSONEncoding.default,view: view, completion: completion)
        
    }
    
    static func put<T:EVObject>(url: String, parameters: Dictionary<String, Any>, view : UIViewController,authRequired: Bool, completion: @escaping (T) -> Void) {
        baseNetworkOperation(method: .put, url: url, parameters: parameters, encoding: JSONEncoding.default,view: view,completion: completion)
        
    }
    
    static func delete<T:EVObject>(url: String, parameters: Dictionary<String, Any> = [String: String](), view : UIViewController, completion: @escaping (T) -> Void) {
        baseNetworkOperation(method: .delete, url: url, parameters: parameters, view: view,completion: completion)
        
        
    }
    
    //    static private func newBaseNetworkOperation<T:EVObject>(method: HTTPMethod, url: String, parameters: Dictionary<String, Any>?, view : UIViewController,authRequired: Bool, completion: @escaping ([T]) -> Void) {
    //        Loader.startLoading();
    //        var request:DataRequest;
    //        if(authRequired) {
    //            //            let authHeader = [
    //            //                "x-Auth-Token": CommonFunctions.sharedInstance.getValueFromUserDefaultsForKey("TOKEN")! as! String
    //            //            ];
    //            request = Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: authHeader);
    //            print("autherization...", authHeader)
    //        } else {
    //            request = Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default);
    //        }
    //        request.validate()
    //            .responseArray{ (response: DataResponse<[T]>) in
    //                //                if #available(iOS 10.0, *) {
    //                //                    debugPrint(response.metrics ?? "0")
    //                //                }
    //                if let result = response.result.value {
    //                    completion(result)
    //                } else {
    //                    handleError(view: view, response: response)
    //                }
    //                Loader.stopLoading()
    //        }
    //    }
    
    static private func baseNetworkOperation<T:EVObject>(method: HTTPMethod, url: String, parameters: Dictionary<String, Any>, encoding: ParameterEncoding = URLEncoding.default, view : UIViewController,completion: @escaping (T) -> Void) {
        Loader.startLoading(minimumDisplayTime: 0.3, message: NSLocalizedString("Please wait...", comment: "please wait"))//"Bitte Warten...
        //    Loader.startLoading();
        let request:DataRequest;
        //        if(authRequired) {
        //            request = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: authHeader);
        //        } else {
        request = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding);
        // }
        print("REQUEST IS : ")
        print(request)
        request.validate()
            .responseObject{ (response: DataResponse<T>) in
                //                if #available(iOS 10.0, *) {
                //                    debugPrint(response.metrics ?? "0")
                //                }
                if let result = response.result.value {
                    if(result is TokenDTO) {
                        saveToken(view: view, token: result as! TokenDTO)
                    }
                    print(result)
                    completion(result)
                    //                    view.createAlert(title: "",body: jsonResponse["description"] as! String)
                    //                    if let data = result, let utf8Text = String(data: data, encoding: .utf8) {
                    //                        let json = try? JSONSerialization.jsonObject(with: data)
                    //                        print(json!)
                    //               let jsonResponse = result as? NSObject
                    //                    let resultResponse = jsonResponse["reason"] as! String
                    //                    view.createAlert(title: "", body: "\(jsonResponse.)")
                }
                else {
                    handleError(view: view, response: response)
                }
                Loader.stopLoading()
        }
        
    }
    
    //    static private func baseNetworkOperationMethodTest<T:EVObject>(method: HTTPMethod, url: String, parameters: Dictionary<String, Any>, encoding: ParameterEncoding = URLEncoding.default, view : UIViewController,completion: @escaping (T) -> Void) {
    //        Loader.startLoading();
    //        print(parameters);
    //        var request:DataRequest;
    ////        if(authRequired) {
    ////            request = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: authHeader);
    ////        } else {
    ////            request = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding);
    ////        }
    //        print("REQUEST IS : ")
    //        print(request)
    //        request.validate().response{ response in
    //            print("asedrjk.....\(response)");
    ////            switch response.result {
    ////            case .Success(let data):
    ////
    ////                print(name)
    ////            case .Failure(let error):
    ////                print("Request failed with error: \(error)")
    ////            }
    //            Loader.stopLoading();
    //        }
    //
    //    }
    
    
    static private func baseNetworkOperationMethod<T:EVObject>(method: HTTPMethod, url: String, parameters: Dictionary<String, Any>, encoding: ParameterEncoding = URLEncoding.default, view : UIViewController,  completion: @escaping (T) -> Void) {
        Loader.startLoading();
        var request:DataRequest;
        //        if(authRequired) {
        request = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: authHeader);
        //        } else {
        //            request = Alamofire.request(url, method: method, parameters: parameters, encoding: encoding);
        //        }
        print("REQUEST IS : ")
        print(request)
        request.validate()
            
            .responseObject{ (response: DataResponse<T>) in
                //                if #available(iOS 10.0, *) {
                //                    debugPrint(response.metrics ?? "0")
                //                }
                if let result = response.result.value {
                    if(result is TokenDTO) {
                        saveToken(view: view, token: result as! TokenDTO)
                    }
                    print(result)
                    completion(result)
                    //                    view.createAlert(title: "",body: jsonResponse["description"] as! String)
                    //                    if let data = result, let utf8Text = String(data: data, encoding: .utf8) {
                    //                        let json = try? JSONSerialization.jsonObject(with: data)
                    //                        print(json!)
                    //               let jsonResponse = result as? NSObject
                    //                    let resultResponse = jsonResponse["reason"] as! String
                    //                    view.createAlert(title: "", body: "\(jsonResponse.)")
                }
                else {
                    handleError(view: view, response: response)
                }
                Loader.stopLoading()
        }
        
    }
    
    
    
    static private func handleError<T>(view: UIViewController, response: DataResponse<T>) {
        
        
        
        var statusCode = response.response?.statusCode
        if response.result.isFailure {
            
            //??NETWORK ERROR OR INVALID SERVER RESPONSE??
            
            view.createAlert(title: "OOPS!!", body: "Mit network Vernetzen...")
            
        }
        if let error = response.result.error as? AFError {
            statusCode = error._code // statusCode private
            switch error {
            case .invalidURL(let url):
                print("Invalid URL: \(url) - \(error.localizedDescription)")
            case .parameterEncodingFailed(let reason):
                print("Parameter encoding failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
            case .multipartEncodingFailed(let reason):
                print("Multipart encoding failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
            case .responseValidationFailed(let reason):
                print("Response validation failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
                
                switch reason {
                case .dataFileNil, .dataFileReadFailed:
                    print("Downloaded file could not be read")
                case .missingContentType(let acceptableContentTypes):
                    print("Content Type Missing: \(acceptableContentTypes)")
                case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                    print("Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
                case .unacceptableStatusCode(let code):
                    print("Response status code was unacceptable: \(code)")
                    statusCode = code
                }
                
                if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                    let json = try? JSONSerialization.jsonObject(with: data)
                    print(json ?? "dfghj")
                    
                    if let jsonResponse = json as? NSDictionary {
                        if let errorFields = jsonResponse["fieldErrors"] as? NSArray {
                            if errorFields.count == 0{
                                print("error fields are zero")
                                view.createAlert(title: "Error",body: jsonResponse["description"] as! String)
                                
                            }
                            else{
                                let message = ((errorFields[0] as! NSDictionary)["message"]) as! String
                                
                                print("message......\(message)")
                                view.createAlert(title: "Error", body: "\(message)" )
                            }
                            print("no alert....")
                            //                            view.createAlert(title: "", body: jsonResponse["description"] as! String)
                        }
                        if jsonResponse["description"] as? String == nil {
                            print("null......")
                        }
                        else{
                            view.createAlert(title: "Error", body: jsonResponse["description"] as! String)
                        }
                    }
                    
                    print("Utf : " + utf8Text)
                    //let errorDTO: ErrorDTO = ErrorDTO(json: utf8Text);
                    //                    view.createAlert(title: "", body: jsonResponse["description"] as! String)
                    
                    
                    
                    
                }
                
            case .responseSerializationFailed(let reason):
                print("Response serialization failed: \(error.localizedDescription)")
                print("Failure Reason: \(reason)")
                view.createAlert(title: "Failed!", body: "Serialization failed")
                // statusCode = 3840 ???? maybe..
            }
            
            print("Underlying error: \(error.underlyingError)")
        } else if let error = response.result.error as? URLError {
            print("URLError occurred: \(error)")
        }
        else {
            print("Unknown error: \(response.result.error)")
        }
        
        print(statusCode ?? "sdfghj")
        //print(statusCode ?? 200) // the status code
        
    }
    
    static private func saveToken(view: UIViewController, token: TokenDTO){
        let preferences = UserDefaults.standard
        
        preferences.setValue(token.token, forKey: Service.hunger_hub)
        let didSave = preferences.synchronize()
        
        if !didSave {
            view.createAlert(title: "Error", body: "Unable to save token")
        }
        loadTokenOnAppLoad()
    }
    
    static func logout(view: UIViewController) {
        
    Loader.startLoading(minimumDisplayTime: 0.7, message: NSLocalizedString("Please wait...", comment: ""))
        //        let appDomain = Bundle.main.bundleIdentifier!
        //        UserDefaults.standard.removePersistentDomain(forName: appDomain)
        //        Loader.stopLoading()
        
        UserDefaults.standard.setValue(nil, forKey: ProfileViewController.email)
        UserDefaults.standard.set(nil, forKey: "AppleLanguage")
        UserDefaults.standard.synchronize()
        Loader.stopLoading()
        //        let preferences = UserDefaults.standard
        //
        //        preferences.setValue(nil, forKey: Service.hunger_hub)
        //        preferences.synchronize()
        //        userToken = nil
        //        authHeader = HTTPHeaders()
    }
    
    //    static internal func convertJSONToObject<T: EVObject>(_ keyPath: String?, mapToObject object: T? = nil) -> DataResponseSerializer<T> {
    //        return DataResponseSerializer { request, response, data, error in
    //            guard error == nil else {
    //                return .failure(error!)
    //            }
    //
    //            guard let _ = data else {
    //                let failureReason = "Data could not be serialized. Input data was nil."
    //                let error = self.newError(.noData, failureReason: failureReason)
    //                return .failure(error)
    //            }
    //
    //            let jsonResponseSerializer = DataRequest.jsonResponseSerializer(options: .allowFragments)
    //            let result = jsonResponseSerializer.serializeResponse(request, response, data, error)
    //            if result.value == nil {
    //                let failureReason = "Data could not be serialized. Input data was not json."
    //                let error = self.newError(.noData, failureReason: failureReason)
    //                return .failure(error)
    //            }
    //
    //            var JSONToMap: NSDictionary?
    //            if let keyPath = keyPath , keyPath.isEmpty == false {
    //                JSONToMap = (result.value as AnyObject?)?.value(forKeyPath: keyPath) as? NSDictionary
    //            } else {
    //                JSONToMap = result.value as? NSDictionary
    //            }
    //            if JSONToMap == nil {
    //                JSONToMap = NSDictionary()
    //            }
    //            if response?.statusCode ?? 0 > 300 {
    //                let newDict = NSMutableDictionary(dictionary: JSONToMap!)
    //                newDict["__response_statusCode"] = response?.statusCode ?? 0
    //                JSONToMap = newDict
    //            }
    //
    //            if object == nil {
    //                let instance: T = T()
    //                let parsedObject: T = ((instance.getSpecificType(JSONToMap!) as? T) ?? instance)
    //                let _ = EVReflection.setPropertiesfromDictionary(JSONToMap!, anyObject: parsedObject)
    //                return .success(parsedObject)
    //            } else {
    //                let _ = EVReflection.setPropertiesfromDictionary(JSONToMap!, anyObject: object!)
    //                return .success(object!)
    //            }
    //        }
    //    }
    //    
    //    enum ErrorCode: Int {
    //        case noData = 1
    //    }
    //    
    //    static internal func newError(_ code: ErrorCode, failureReason: String) -> NSError {
    //        let errorDomain = "com.hocrox.error.error"
    //        
    //        let userInfo = [NSLocalizedFailureReasonErrorKey: failureReason]
    //        let returnError = NSError(domain: errorDomain, code: code.rawValue, userInfo: userInfo)
    //        return returnError
    //    }
}
