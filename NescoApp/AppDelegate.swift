//
//  AppDelegate.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 13/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

//import UIKit
//import Material
//import UserNotifications
//import Firebase
//import FirebaseCore
//import FirebaseMessaging
//
//var deviceTokenApp = "User cancel to register"
//var user_id = 0
//var assitentId = 0
//var windowGlobal: UIWindow?   //UNUserNotificationCenterDelegate
//@UIApplicationMain
//class AppDelegate: UIResponder, UIApplicationDelegate {
//
//    var window: UIWindow?
//    var deviceTokenString: String!
//    let gcmMessageIDKey = "gcm.message_id"
//    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
//        
//        windowGlobal = window
//      //  registerForRemoteNotification()
//        application.applicationIconBadgeNumber = 0
//        FIRApp.configure()
//        FIRMessaging.messaging().delegate = self
//        
//        if #available(iOS 10.0, *) {
//        UNUserNotificationCenter.current().delegate = self
//        
//        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//        UNUserNotificationCenter.current().requestAuthorization(
//            options: authOptions,
//            completionHandler: {_, _ in })
//    } else {
//    let settings: UIUserNotificationSettings =
//    UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//    application.registerUserNotificationSettings(settings)
//    }
//    
//    application.registerForRemoteNotifications()
//        
//    return true
//    }
//    
//    
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
//                if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
//        }
//    
//        print(userInfo)
//    }
//    
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
//                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//                if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
//        }
//        
//       
//        print(userInfo)
//        
//        completionHandler(UIBackgroundFetchResult.newData)
//    }
//   
//    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
//        print("Unable to register for remote notifications: \(error.localizedDescription)")
//    }
//    
//        func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//            
//            deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
//            print("device messaging id.....\(deviceTokenString)")
//        //print("APNs token retrieved: \(deviceToken)")
//    }
//
//       func applicationWillResignActive(_ application: UIApplication) {
//        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
//        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
//    }
//
//    func applicationDidEnterBackground(_ application: UIApplication) {
//        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
//        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
//    }
//
//    func applicationWillEnterForeground(_ application: UIApplication) {
//        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
//    }
//
//    func applicationDidBecomeActive(_ application: UIApplication) {
//        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//    }
//
//    func applicationWillTerminate(_ application: UIApplication) {
//        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
//    }
//   
//}
//@available(iOS 10, *)
//extension AppDelegate : UNUserNotificationCenterDelegate {
//    
//    // Receive displayed notifications for iOS 10 devices.
//    func userNotificationCenter(_ center: UNUserNotificationCenter,
//                                willPresent notification: UNNotification,
//                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        let userInfo = notification.request.content.userInfo
//        
//        
//        if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
//        }
//        
//        // Print full message.
//        print(userInfo)
//        
//        completionHandler([])
//    }
//    
//    func userNotificationCenter(_ center: UNUserNotificationCenter,
//                                didReceive response: UNNotificationResponse,
//                                withCompletionHandler completionHandler: @escaping () -> Void) {
//        let userInfo = response.notification.request.content.userInfo
//        // Print message ID.
//        if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
//        }
//        
//        // Print full message.
//        print(userInfo)
//        
//        completionHandler()
//    }
////    func application(application: UIApplication,
////                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
////        Messaging.messaging().apnsToken = deviceToken
////    }
////
//}
//extension AppDelegate : FIRMessagingDelegate {
//    // [START refresh_token]
//    func messaging(_ messaging: FIRMessaging, didRefreshRegistrationToken fcmToken: String) {
//        print("Firebase registration token: \(fcmToken)")
//    }
//    // [END refresh_token]
//    // [START ios_10_data_message]
//    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
//    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
//    func messaging(_ messaging: FIRMessaging, didReceive remoteMessage: FIRMessagingRemoteMessage) {
//        print("Received data message: \(remoteMessage.appData)")
//    }
//    
//    public func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
//        print("Firebase registration token: \(remoteMessage)")
//
//    }
//       // [END ios_10_data_message]
//}



import UIKit
import UserNotifications
import IQKeyboardManagerSwift

import Firebase

var deviceTokenApp = "User Canceled to register"
var fcm_Token = "Not have fcm key"
var windowGlobal: UIWindow?
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    static let assistentId = "assistentId"
    static let userId = "userId"
    static let typeOfMsg = "type"
    static let fcm_token = "type"
    var array1: [String] = []
    var deviceTokenString: String!
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.sharedManager().enable = true
        FirebaseApp.configure()
        
        windowGlobal = window
        
        // [START set_messaging_delegate]
        application.applicationIconBadgeNumber = 0
        Messaging.messaging().delegate = self
        
        // [END set_messaging_delegate]
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        // [END register_for_notifications]
        handleLanguage()
        return true
    }
    
    func handleLanguage(){
        if UserDefaults.standard.string(forKey: ProfileViewController.email) != nil{
            
            let firstController: ListOfContactViewController =
            {  return UIStoryboard.viewController(identifier: "ListOfContactViewController") as! ListOfContactViewController
            }()
            firstController.tap = true
            if(firstController.tabBar != nil) {
                firstController.tabBar.selectedIndex = 0;
            }
            
            let appToolbarController1 = AppToolbarController(rootViewController: firstController)
            
            windowGlobal!.rootViewController = appToolbarController1
            windowGlobal!.makeKeyAndVisible()
        }
        else{
            if UserDefaults.standard.object(forKey: "AppleLanguage") != nil {
                
                let firstController: ListOfContactViewController =
                {  return UIStoryboard.viewController(identifier: "ListOfContactViewController") as! ListOfContactViewController
                }()
                firstController.tap = true
                if(firstController.tabBar != nil) {
                    firstController.tabBar.selectedIndex = 0;
                }
                
                let appToolbarController1 = AppToolbarController(rootViewController: firstController)
                
                windowGlobal!.rootViewController = appToolbarController1
                windowGlobal!.makeKeyAndVisible()
                
            }
            else{
                
            let firstController: ViewController =
            {  return UIStoryboard.viewController(identifier: "ViewController") as! ViewController
            }()
            
            let appToolbarController1 = AppToolbarController(rootViewController: firstController)
            
            windowGlobal!.rootViewController = appToolbarController1
            windowGlobal!.makeKeyAndVisible()
            }
        }
        
        
    }
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    // [END receive_message]
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("device messaging id.....\(deviceTokenString)")
        //deviceTokenApp = deviceTokenString
        
       // print("APNs token retrieved: \(deviceToken)")
        
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }
}
func forwardTokenToServer(_ token: String){
    
    UserDefaults.standard.setValue(token, forKey: "Token")
    print("Device Token  : \(token)")
    
    
    
}


// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print("user info..... \n \(userInfo) \n")
        
        print("notification.......\n \(userInfo["assistent_id"] as! String) \n")
        let assistentID = userInfo["assistent_id"] as! String
        print("user_id.......\n \(userInfo["user_id"] as! String) \n")
         let userID = userInfo["user_id"] as! String
         print("type.......\n \(userInfo["type"] as! String) \n")
         let typeOfMsg = userInfo["type"] as! String
        print("mess_id.......\n \(userInfo["mess_id"] as! String) \n")
        let msgId = userInfo["mess_id"] as! String
//         print("reply_message.......\n \(userInfo["reply_message"] as! String) \n")
//         let sendMsg = userInfo["reply_message"] as! String
         print("message.......\n \(userInfo["message"] as! String) \n")
         let message = userInfo["message"] as! String
         print("name.......\n \(userInfo["name"] as! String) \n")
         let name = userInfo["name"] as! String
        
        for (key, value) in UserDefaults.standard.dictionaryRepresentation() {
            print("\(key) = \(value) \n")
        }

        let dict = userInfo as Dictionary
        print("dictionary......\(dict)")
        
        UserDefaults.standard.set(dict, forKey: "data")
        
        UserDefaults.standard.setValue(assistentID, forKey: AppDelegate.assistentId)
        UserDefaults.standard.setValue(userID, forKey: AppDelegate.userId)
      //  UserDefaults.standard.setValue(typeOfMsg, forKey: AppDelegate.typeOfMsg)
        UserDefaults.standard.synchronize()

       // presentNextView(userID,assistentId: assistentID)
        
        let newData = NotifictnData(userId: userID, message: message, name: name)
        var dataArray = [Data]()
        let savedData = NSKeyedArchiver.archivedData(withRootObject: newData)
        dataArray.append(savedData)
        print("dataArray: \(dataArray))")
        print("savedData: \(savedData))")
                if  UserDefaults.standard.object(forKey: "\(assistentID)") != nil {
            print(UserDefaults.standard.object(forKey: "\(assistentID)"))
            var explodedData = UserDefaults.standard.object(forKey: "\(assistentID)") as! [Data];
            explodedData.append(savedData);
            UserDefaults.standard.set(explodedData, forKey: "\(assistentID)")
        } else {
            var explodedData = [Data]();
            explodedData.append(savedData);
            UserDefaults.standard.set(explodedData, forKey: "\(assistentID)")
        }
        
        
        UserDefaults.standard.synchronize()
        completionHandler([])
    }
    
    func presentNextView(_ userId: String, assistentId: String, name: String){
        
    let vc = {return UIStoryboard.viewController(identifier: "ChatViewController") as! ChatViewController}()
        vc.bck = true
        vc.asstntID = assistentId
        //vc.name
        vc.usrID = userId
        vc.isFromNotification = true
        openNewWindow = UIWindow(frame: UIScreen.main.bounds)
        openNewWindow.backgroundColor = UIColor.red
        openNewWindow.windowLevel = UIWindowLevelAlert + 1
        openNewWindow.rootViewController = vc
        openNewWindow.makeKeyAndVisible()
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
       // print(userInfo)
    
        
        print("notification.......\n \(userInfo["assistent_id"] as! String) \n")
        let assistentID = userInfo["assistent_id"] as! String
        print("user_id.......\n \(userInfo["user_id"] as! String) \n")
        let userID = userInfo["user_id"] as! String
        print("type.......\n \(userInfo["type"] as! String) \n")
        let typeOfMsg = userInfo["type"] as! String
//        print("send_message.......\n \(userInfo["send_mess"] as! String) \n")
//        let sendMsg = userInfo["send_mess"] as! String
        print("message.......\n \(userInfo["message"] as! String) \n")
        let message = userInfo["message"] as! String
        print("name.......\n \(userInfo["name"] as! String) \n")
        let name = userInfo["name"] as! String
        let dict = userInfo as Dictionary
        print("dictionary......\(dict)")
        UserDefaults.standard.setValue(assistentID, forKey: AppDelegate.assistentId)
        UserDefaults.standard.setValue(userID, forKey: AppDelegate.userId)
        UserDefaults.standard.array(forKey: AppDelegate.userId)
        //UserDefaults.standard.setValue(typeOfMsg, forKey: AppDelegate.typeOfMsg)
        
        UserDefaults.standard.synchronize()
        array1.append(assistentID)
        
        print("array is....\(array1)")
        
        
//        let explodedData = UserDefaults.standard.object(forKey: "\(assistentID)") as! [Data];
//        for dat in explodedData {
//            let notiData = NSKeyedUnarchiver.unarchiveObject(with: dat) as!NotifictnData;
//            
//            print("notiData....\(notiData)")
//            
//            
//            
//            print("message is \(notiData.message)");
//        }
        presentNextView(userID,assistentId: assistentID,name: name)
    
        completionHandler()
    }
}


extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        UserDefaults.standard.setValue(fcmToken, forKey: AppDelegate.fcm_token)
        //fcm_Token = fcmToken
      
    }
    // [END refresh_token]
    
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}

class NotifictnData: NSObject,NSCoding{
    
    var user_id: String = ""
    
    var message: String = ""
    
    var name: String = ""
    
    init(userId: String, message: String, name: String) {
        self.user_id = userId
        self.name = name
        self.message = message
    }

    required convenience init(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: "name") as! String
        let user_id = aDecoder.decodeObject(forKey: "user_id") as! String
        let message = aDecoder.decodeObject(forKey: "message") as! String
        self.init(
            userId: user_id,
            message: message,
            name: name
        )
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(user_id, forKey: "user_id")
        aCoder.encode(message, forKey: "message")
        aCoder.encode(name, forKey: "name")
    }
    
}
    


