//
//  RegisterDeviceVM.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 04/07/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation

class RegisterDeviceVM{
    
    var email = ""
   var device_id = ""
   var user_type = ""
   var google_key = ""
   var device_type = "IOS"
    
    
    public func toJSON()-> Dictionary<String, String>{
        
        return[
        
            "email": self.email,
            "device_id": self.device_id,
            "user_type": self.user_type,
            "google_Key": self.google_key,
            "device_type": self.device_type
        ]
    
    }
}

