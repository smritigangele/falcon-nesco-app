//
//  GetAssistentVM.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 17/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation

class GetAssistentVM{
    
    var email = ""
    
    public func toJSON()-> Dictionary<String, String>{
        
        return[
            
            "email": self.email
        ]
    }
}
