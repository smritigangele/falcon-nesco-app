//
//  ChatVM.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 21/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation

class ChatVM{
    
    var user_id = 0
    var assistent_id = 0
    
    public func toJSON()-> Dictionary<String, String>{
        return[
            
            "to_id": "\(self.user_id)",
            "from_id": "\(self.assistent_id)"
        ]
}
    
}
