
//
//  SendChatMsgsVM.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 21/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation

class SendChatMsgsVM{
    
    var user_id                   =     ""
    var assistent_id              =     ""
    var message                   =     ""
    
    public func toJSON()-> Dictionary<String, String>{
        
        return[
            
            "user_id": self.user_id,
            "assistent_id": self.assistent_id,
            "message": self.message
            
            
        ]
        
        
        
    }
    
    
    
}
