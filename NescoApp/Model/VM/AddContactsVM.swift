//
//  AddContactsVM.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 17/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation

class AddContactsVM{
    var email  =  ""
    var user_email  =  ""
    var name  =  ""
    var phone  =  ""
    
    public func toJSON()-> Dictionary<String, String>{
        return[
        
        "email": self.email,
        "user_email": self.user_email,
        "name": self.name,
        "phone": self.phone
            
        ]
        
    }
}
