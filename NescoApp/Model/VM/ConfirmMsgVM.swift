//
//  ConfirmMsgVM.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 22/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class ConfirmMsgVM: EVObject{
    
    var user_id = 0
    var assistent_id = 0
    
    public func toJSON()-> Dictionary<String, String>{
        
        return[
        
        "user_id": "\(self.user_id)",
        "assistent_id": "\(self.assistent_id)"
        
        ]
        
        
        
    }
    
    
    
    
}
