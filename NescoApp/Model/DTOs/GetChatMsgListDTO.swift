//
//  GetChatMsgListDTO.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 21/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class GetChatMsgListDTO: EVObject{
    
    var id = 0
    var user_id = 0
    var assistent_id = 0
    var message = ""
    var from_mess = ""
    var datetime = ""
    var date = ""
    var time = ""
    
}
