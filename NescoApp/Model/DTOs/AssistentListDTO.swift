//
//  AssistentListDTO.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 17/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class AssistentListDTO: EVObject{
    
    var assistent_id         = ""
    var category             = ""
    var category_eng         = ""
    var company              = ""
    var email                = ""
    var logo                 = ""
    var phone                = ""
    var message              = ""
    var datetime             = ""
    var date                 = ""
    var time                 = ""
//    var city                 = ""
}
