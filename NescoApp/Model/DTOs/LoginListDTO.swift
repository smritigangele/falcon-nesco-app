//
//  LoginListDTO.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 17/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class LoginListDTO: EVObject{
    
    var assistent_id       =      0
    var category           =      ""
    var company            =      ""
    var email              =      ""
    var logo               =      ""
    var phone              =      ""

}
