//
//  GetContactsListDTO.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 22/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class GetContactsListDTO: EVObject{
    
    var id = ""
    var email = ""
    var user_type = ""
    var device_id = ""
    var google_key = ""
    var mem_comp_id = ""
    var firstname = ""
    var user_email = ""
    var address = ""
    var name = ""
    var phone = ""
    var city = ""
    var device_type = ""
}
