//
//  TokenDTO.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 15/09/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class TokenDTO: EVObject {
    var token: String = "";
    var timestamp: String = "";
}
