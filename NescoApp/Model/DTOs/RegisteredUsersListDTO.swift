//
//  RegisteredUsersListDTO.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 17/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class RegisteredUsersListDTO: EVObject{

    var id           =       ""
    var email        =       ""
    var user_email   =       ""
    var name         =       ""
    var phone        =       ""
    
}
