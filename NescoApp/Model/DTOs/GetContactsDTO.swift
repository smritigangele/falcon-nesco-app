//
//  GetContactsDTO.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 22/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class GetContactsDTO: EVObject{
    
  var status = ""
  var result = Array<GetContactsListDTO>()
}
