//
//  RegisteredUsersDTO.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 17/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class RegisteredUsersDTO: EVObject{
    
    var status = ""
    var result : Array<RegisteredUsersListDTO> = Array<RegisteredUsersListDTO>()
    
    
}
