//
//  GetChatMsgDTO.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 21/06/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import EVReflection

class GetChatMsgDTO: EVObject{
    
    var status = ""
    var result: Array<GetChatMsgListDTO> = Array<GetChatMsgListDTO>()
    
    
}
