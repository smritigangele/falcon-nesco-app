//
//  HTextField.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 15/09/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
import Material

class HTextField: TextField, UITextFieldDelegate {
    
    static let navyBlue = UIColor(red: 28/255, green: 93/255, blue: 141/255, alpha: 1);
    static let lightBlue = UIColor(red: 41/255, green: 105/255, blue: 152/255, alpha: 1);
    static let green = UIColor(red: 0/255, green: 121/255, blue: 107/255, alpha: 1);
    
    private var maxLengths = [UITextField: Int]()
    
    @IBInspectable var allowedChars: String = ""
    @IBInspectable var bannedChars: String = ""
    @IBInspectable var maxLength: Int {
        get {
            guard let length = maxLengths[self] else {
                return Int.max
            }
            return length
        }
        set {
            maxLengths[self] = newValue
            addTarget(
                self,
                action: #selector(limitLength),
                for: UIControlEvents.editingChanged
            )
        }
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        delegate = self
        autocorrectionType = .no
        self.font = UIFont.appFont
        self.tintColor = Color.appGrey
        //        placeholderActiveColor = Color.appGreen
        dividerActiveColor = Color.lightGray
        self.adjustsFontSizeToFitWidth = false;
        //intrinsicContentSize = CGSize(width: width, height: 20)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        delegate = self
        autocorrectionType = .no
        self.font = UIFont.appFont
        self.tintColor = Color.appGrey
        //        placeholderActiveColor = Color.appGreen
        dividerActiveColor = Color.lightGray
        self.adjustsFontSizeToFitWidth = false;
        //        intrinsicContentSize = CGSize(width: width, height: 20)
    }
    
    func limitLength(textField: UITextField) {
        guard let prospectiveText = textField.text, prospectiveText.characters.count > maxLength else {
            return
        }
        let selection = selectedTextRange
        text = prospectiveText.substring(with: Range<String.Index>(prospectiveText.startIndex ..< prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)))
        selectedTextRange = selection
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard string.characters.count > 0 else {
            return true
        }
        
        let currentText = textField.text ?? ""
        let prospectiveText = (currentText as NSString).replacingCharacters(in: range, with: string)
        var allowed = true;
        var banned = true;
        if allowedChars.characters.count > 0 {
            allowed = prospectiveText.containsOnlyCharactersIn(matchCharacters: allowedChars)
        }
        if bannedChars.characters.count > 0 {
            banned = prospectiveText.doesNotContainCharactersIn(bannedChars)
        }
        return allowed && banned
    }
}
