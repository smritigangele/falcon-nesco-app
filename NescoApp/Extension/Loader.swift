//
//  Loader.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 15/09/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import NVActivityIndicatorView


class Loader {
    
    static func startLoading(minimumDisplayTime:Double = 0.2,message: String = NSLocalizedString("Please wait...", comment: "")){
        NVActivityIndicatorView.DEFAULT_TYPE = .ballSpinFadeLoader
        NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
        let activityData = ActivityData(message: message)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    static func stopLoading(){
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
}
