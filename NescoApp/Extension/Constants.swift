//
//  Constants.swift
//  Plenty of Freight
//
//  Created by Bhawani Singh on 01/11/16.
//  Copyright © 2016 Hocrox Infotech Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

class Constants {
    
   static let baseURL: String = "http://assistent24.ch/supplier/";
   static  let loginURL: String = Constants.baseURL + "login_supplier.php";
   static  let getAssistent: String = Constants.baseURL + "supplier_login_count.php?email=";
   static  let registeredUsers: String = Constants.baseURL + "get_customer_list.php"
   static  let myContacts: String = Constants.baseURL + "get_contact.php?"
    static  let myContacts_1: String = Constants.baseURL + "get_contact_1.php?"
   static  let getServiceMssgs: String = Constants.baseURL + "my_chat_messages.php?user_id="
   static  let addContacts: String = Constants.baseURL + "add_contact.php?"
   static  let getChatMessgs: String = Constants.baseURL + "get_chat_messages.php?"
   static  let sendChatMsgs: String = Constants.baseURL + "send_chat_mess.php?"
   static  let confirmMsg: String = Constants.baseURL + "confirm_services.php?"
   static  let getCustomer: String = Constants.baseURL + "get_customer_list.php"
   static  let updateDevice: String = Constants.baseURL + "update_regid.php?"
   }
