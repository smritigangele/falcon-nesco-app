//
//  Extension.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 15/09/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
import Material

extension UIStoryboard {
    class func viewController(identifier: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier)
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func createAlert(title: String, body: String) {
        let alert = UIAlertController(title: title, message: body , preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
        }))
        self.present(alert, animated: true, completion: nil)
        
        //        self.view.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
}

extension Color {
    open static let appRed = UIColor(red: 223/255, green: 86/255, blue: 54/255, alpha: 1);
    open static let appGreen = UIColor(red: 27/255, green: 181/255, blue: 131/255, alpha: 1);
    open static let appYellow = UIColor(red: 249/255, green: 148/255, blue: 30/255, alpha: 1);
    open static let appGrey = UIColor(red: 132/255, green: 132/255, blue: 132/255, alpha: 1);
    open static let appLightGrey = UIColor(red: 132/255, green: 132/255, blue: 132/255, alpha: 1);
    open static let appRedDeactivate = UIColor(red: 250/255, green: 169/255, blue: 150/255, alpha: 1);
    
}


extension UIFont {
    open static let appMinuteFont = UIFont.systemFont(ofSize: CGFloat(Double.appMinuteSize));
    open static let appTinyFont = UIFont.systemFont(ofSize: CGFloat(Double.appTinySize));
    open static let appFont = UIFont.systemFont(ofSize: CGFloat(Double.appSize));
    open static let appTextFont = UIFont.systemFont(ofSize: CGFloat(Double.appTextSize));
    open static let appHeadingFont = UIFont.systemFont(ofSize: CGFloat(Double.appHeadingSize), weight: UIFontWeightMedium);
}

extension Double {
    public static let appMinuteSize: Double = 10;
    public static let appTinySize: Double = 12;
    public static let appSize: Double = 14;
    public static let appTextSize: Double = 16;
    public static let appHeadingSize: Double = 20;
}

extension String {
    
    // Returns true if the string contains only characters found in matchCharacters.
    func containsOnlyCharactersIn(matchCharacters: String) -> Bool {
        let disallowedCharacterSet = NSCharacterSet(charactersIn: matchCharacters).inverted
        return self.rangeOfCharacter(from: disallowedCharacterSet) == nil
    }
    
    // Returns true if the string has no characters in common with matchCharacters.
    func doesNotContainCharactersIn(_ matchCharacters: String) -> Bool {
        let characterSet = CharacterSet(charactersIn: matchCharacters)
        return self.rangeOfCharacter(from: characterSet) == nil
    }
}
