//
//  RootViewController.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 15/09/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import UIKit
import Material

class RootViewController: UIViewController {
    open override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Color.black
        
        prepareToolbar()
    }
}

extension RootViewController {
    fileprivate func prepareToolbar() {
        guard let tc = toolbarController else {
            return
        }
        
        tc.toolbar.title = "Nesco"
        tc.toolbar.detail = "An assistent app"
    }
}

