//
//  AppToolbarController.swift
//  
//
//  Created by Hocrox Infotech Pvt Ltd1 on 12/07/17.
//
//

import Foundation
import UIKit
import Material

class AppToolbarController: ToolbarController {
    static let navyBlue = UIColor(red: 28/255, green: 93/255, blue: 141/255, alpha: 1);
    static let lightBlue = UIColor(red: 41/255, green: 105/255, blue: 152/255, alpha: 1);
    
    static let newgreen = UIColor(red: 0/255, green: 121/255, blue: 107/255, alpha: 1)
    static let newBlue = UIColor(red: 42/255, green: 97/255, blue: 143/255, alpha: 1)
    fileprivate var menuButton: IconButton!
    fileprivate var switchControl: Switch!
    fileprivate var moreButton: IconButton!
    
    override func prepare() {
        super.prepare()
        prepareMenuButton()
        prepareStatusBar()
        prepareToolbar()
    }
}

extension AppToolbarController {
    fileprivate func prepareMenuButton() {
        menuButton = IconButton(image: Icon.cm.menu, tintColor: .white)
        menuButton.addTarget(self, action: #selector(handleMenuButton), for: .touchUpInside)
    }
    
    fileprivate func prepareStatusBar() {
        
        statusBar.backgroundColor = AppToolbarController.newgreen
        
    }
    
    fileprivate func prepareToolbar() {
        toolbar.backgroundColor = AppToolbarController.lightBlue
        toolbar.height = 0
        //        toolbar.titleLabel.textColor = Color.white
        //        toolbar.leftViews = [menuButton]
    }
}

extension AppToolbarController {
    @objc
    fileprivate func handleMenuButton() {
        navigationDrawerController?.toggleLeftView()
    }
}
