//
//  CustomViewForLanguage.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 17/11/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation

import UIKit

class CustomViewForLanguage: UIView{
    
    @IBOutlet var chooseLng: UIButton!
    
    @IBOutlet var germanLng: UIButton!
   
    @IBOutlet var engLngSelected: UIImageView!
    
    @IBOutlet var OKBtn: UIButton!
    @IBOutlet var cancelBtn: UIButton!
    
    @IBOutlet var germanSelected: UIImageView!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "CustomViewForLanguage", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
    }

}

