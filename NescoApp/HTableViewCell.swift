//
//  HTableViewCell.swift
//  NescoApp
//
//  Created by Hocrox Infotech Pvt Ltd1 on 26/10/17.
//  Copyright © 2017 Hocrox Infotech Pvt Ltd1. All rights reserved.
//

import Foundation
import Material

class HTableViewCell: TableViewCell {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        layoutMargins = UIEdgeInsets.zero
        separatorInset = UIEdgeInsets.zero
        
    }
    
    
}
